import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
		//With indexing, we can access all place on the board.
		printBoard(board); //This is the method in order to display TicTacToe board.

		int moveCount = 0;
		int currentPlayer = 0;

		while (moveCount < 9) {//There are just 9 blanks.

			System.out.print("Player " + (currentPlayer + 1) + " enter row number: ");
			int row = reader.nextInt();
			System.out.print("Player " + (currentPlayer + 1) + " enter column number: ");
			int col = reader.nextInt();
			if (row > 0 && row < 4 && col > 0 && col < 4 && board[row - 1][col - 1] == ' ') {
				if (currentPlayer == 0)
					board[row - 1][col - 1] = 'X';
				else
					board[row - 1][col - 1] = 'O';
				printBoard(board);//After the move, program will display the rearranged board.

				boolean win = checkBoard(board, row - 1, col - 1);
				if (win){
					System.out.println("Player " + (currentPlayer + 1) + " has won the game");
					break;
				}
				currentPlayer = (currentPlayer + 1) % 2;//We use 0 and 1 for setting to another player.(not 1 and 2)
				moveCount++;

			}else{//If a placed is taken already or out of board, then this condition will be satisfied.
				System.out.println("This location is not valid, please enter a new location: ");
			}

		}
		reader.close();
	}

	public static boolean checkBoard(char[][] board, int row, int col){
			char symbol = board[row][col];

			boolean win =true;

			//This is for horizontal check. Row will stay same.
			for (int i = 0; i<3; i++){
				if(symbol != board[row][i]){
					win = false;
				}
			}
			if (win)
				return true;

			//This is for vertical check. Column will stay same.
			win = true;
			for (int j=0; j<3; j++){
				if(symbol != board[j][col]){
					win = false;
				}

			}

			if (win)
				return true;


			//This is for diagonal check from upper left corner to bottom right corner.
			if (row == col){ //0,0  1,1  2,2
				win = true;
				for (int i=0, j=0; i<3; i++,j++){
					if(symbol != board[i][j]){
						win = false;
					}

				}

				if (win)
					return true;
			}

			//This is for diagonal check from bottom left corner to upper right corner.
			if(row + col == 2){ //2,0  1,1  0,2
				win = true;
				for (int i=2, j=0; j<3; i--,j++){
					if(symbol != board[i][j]){
						win = false;
					}
				}
				if (win)
					return true;
			}

			return win;
	}


	public static void printBoard(char[][] board) { //This part is to create the table.
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}