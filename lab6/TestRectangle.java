public class TestRectangle {

    public static void main(String[] args){

        Point topLeft = new Point(5,5);
        Rectangle rect = new Rectangle(12,13,topLeft);

        System.out.println("Area of rectangle: " + rect.area() + " Perimeter of rectangle: " + rect.perimeter());

        Point[] points = rect.corners();
        for(int i = 0; i < points.length; i++){
            System.out.println("Corner " + i + " at x = " + points[i].xCoord + " at y = " + points[i].yCoord);
            //corner 0 topLeft
            //corner 1 topRight
            //corner 2 bottomLeft
            //corner 3 bottomRight
        }
    }
}
