public class Circle {

    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius; //We write this here for distinction
        this.center = center;
    }

    public double area(){
        return Math.PI * radius * radius;
    }

    public double perimeter(){
        return 2 * Math.PI * radius;
    }

    //sum of radii should be greater than or equal to distance between centers for intersecting
    public boolean intersect(Circle circle){
        return (radius + circle.radius) >= center.distanceFromAPoint(circle.center);
    }
}
