public class GCDRec {

    public static void main(String[] args){
        int num1 = Integer.parseInt(args[0]);//to take argument
        int num2 = Integer.parseInt(args[1]);
        int result = gcd(num1, num2);

        System.out.println("GCD of " + num1 + " and " + num2 + " is equal to " + result);

    }

    //gcd(num1, num2) = gcd(num2, num1 % num2)

    private static int gcd(int num1, int num2) {
        int remainder = num1 % num2;
        if (remainder == 0) {
            return num2;
        }
        return gcd(num2, remainder); // It makes the program recursive
    }
}
